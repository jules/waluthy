import re
import base64
import hashlib
import secrets
import requests
from getpass import getpass
from authy_totp import gen_authy_otps
from authy_config import get_value, set_value
from Cryptodome.Cipher import AES

api_endpoint = "https://api.authy.com"
# This is hardcoded into the app and shouldn't really be changed
api_key = "37b312a3d682b823c439522e1fd31c82"
interval = 10


def get_udid():
    udid = get_value("device_udid")
    if not udid:
        udid = secrets.token_hex(8)
        set_value("device_udid", udid)
    return udid


def get_account_status(country, cellphone):
    url = f"{api_endpoint}/json/users/{country}-{cellphone}/status?uuid=authy%3A%3A{get_udid()}&api_key={api_key}&locale=en&device_app=authy"
    respj = requests.get(url).json()
    return respj


def register_device(authy_id, salt, via, device_name):
    # via can be SMS, call, push

    url = f"{api_endpoint}/json/users/{authy_id}/devices/registration/start?signature={salt}&via={via}&api_key={api_key}&locale=en&device_app=authy&device_name={device_name}"
    respj = requests.post(url).json()
    return respj
    # '{"message":"A request was sent to your other devices.","request_id":"request_id_is_here","approval_pin":28,"provider":"push","success":true}'


def get_register_state(authy_id, request_id, salt):
    url = f"{api_endpoint}/json/users/{authy_id}/devices/registration/{request_id}/status?signature={salt}&api_key={api_key}&locale=en&device_app=authy"
    respj = requests.get(url).json()
    # {'message': {'request_status': 'Request Status.'}, 'status': 'pending', 'success': True}
    # {'message': {'request_status': 'Request Status.'}, 'status': 'accepted', 'pin': '123456', 'success': True}
    return respj


def get_account_state(email, country_code, cellphone):
    url = f"{api_endpoint}/json/device/users/status?email={email}&cellphone={cellphone}&country_code={country_code}"
    respj = requests.get(url).json()
    return respj


def finalize_register(authy_id, salt, pin):
    url = f"{api_endpoint}/json/users/{authy_id}/devices/registration/complete?api_key={api_key}&locale=en&device_app=authy"
    respj = requests.post(url, data={"pin": pin, "uuid": f"authy::{get_udid()}"}).json()
    return respj
    # {'device': {'id': 123456789, 'secret_seed': 'secret_seed_goes_here', 'api_key': 'idk_what_this_is_lol', 'reinstall': False}, 'authy_id': 123456}


def verify_secret_seed(device_id, secret_seed: str):
    seedhash = hashlib.sha256(secret_seed.encode()).hexdigest()
    url = f"{api_endpoint}/json/devices/{device_id}/soft_tokens/{device_id}/check?sha={seedhash}&api_key={api_key}&locale=en&device_app=authy"
    respj = requests.get(url).json()
    return respj
    # {'message': 'Token is correct.', 'success': True}
    # {'message': 'Token is damaged. Please reinstall the Authy app.', 'success': False, 'errors': {'message': 'Token is damaged. Please reinstall the Authy app.'}, 'error_code': '60000'}


def aes_decrypt(enc, private_key):
    enc = base64.b64decode(enc)
    iv = b"\x00" * 16  # oh my god whyyyyyyyyyyyyyyy
    cipher = AES.new(private_key, AES.MODE_CBC, iv)
    return cipher.decrypt(enc)


def get_state(authy_id, device_id, secret):
    otps = gen_authy_otps(secret)
    url = (
        f"{api_endpoint}/json/users/{authy_id}/devices/{device_id}?"
        f"api_key={api_key}&locale=en&otp1={otps[0]}&otp2={otps[1]}"
        f"&otp3={otps[2]}&device_id={device_id}"
    )
    respj = requests.get(url).json()
    return respj
    # {'email': 'example@example.com', 'cellphone': '123-456-7890', 'country_code': 90, 'multidevice_enabled': True, 'multidevices_enabled': True, 'success': True}


def do_auth_sync(device_id, secret):
    # &ga_timestamp=1472829734
    otps = gen_authy_otps(secret)
    url = (
        f"{api_endpoint}/json/devices/{device_id}/auth_sync?"
        f"api_key={api_key}&locale=en&otp1={otps[0]}&otp2={otps[1]}"
        f"&otp3={otps[2]}&device_id={device_id}&authy_ver=99&ga_ver=0"
    )
    respj = requests.get(url).json()
    return respj
    # {'moving_factor': '158075892', 'needs_health_check': True, 'sync_ga': True, 'authy_token': {'hidden': True, 'valid': True}, 'update': False, 'sync_password': False, 'enroll_backup_key': True, 'success': True}


def get_devices(authy_id, device_id, secret):
    otps = gen_authy_otps(secret)
    url = (
        f"{api_endpoint}/json/users/{authy_id}/devices?"
        f"api_key={api_key}&locale=en&otp1={otps[0]}&otp2={otps[1]}"
        f"&otp3={otps[2]}&device_id={device_id}"
    )
    respj = requests.get(url).json()
    return respj


def authenticator_tokens(
    user_id, device_id, secret, password=None, interactive_pass=True
):
    otps = gen_authy_otps(secret)
    url = (
        f"{api_endpoint}/json/users/{user_id}/authenticator_tokens?"
        f"apps=&api_key={api_key}&locale=en-US&otp1={otps[0]}&otp2={otps[1]}"
        f"&otp3={otps[2]}&device_id={device_id}"
    )
    tokenj = requests.get(url).json()

    tokens = {}

    for token in tokenj["authenticator_tokens"]:
        if "secret_seed" in token:
            # TODO: Maybe use more than just the name
            tokens[token["name"]] = token["secret_seed"]
            continue
        elif not password:
            if not interactive_pass:
                print("Backups password needed and interactive is disabled.")
                return

            password = getpass("Enter your backups password: ")

        key = hashlib.pbkdf2_hmac(
            "sha1", password.encode(), token["salt"].encode(), 1000, dklen=32
        )
        seed = aes_decrypt(token["encrypted_seed"], key).decode().strip().upper()
        seed = re.sub("[\W_]+", "", seed)
        # TODO: Maybe use more than just the name
        tokens[token["name"]] = {"secret": seed, "type": "totp"}

    # HACK: set value here so if the password was incorrect and things error
    # an improper password won't be saved.
    set_value("backups_password", password)
    return tokens


def get_authy_tokens(user_id, device_id, secret):
    otps = gen_authy_otps(secret)
    url = (
        f"{api_endpoint}/json/users/{user_id}/devices/{device_id}/apps/sync?"
        f"api_key={api_key}&locale=en-US&otp1={otps[0]}&otp2={otps[1]}"
        f"&otp3={otps[2]}&device_id={device_id}"
    )
    tokenj = requests.post(url).json()

    tokens = {}

    for token in tokenj["apps"]:
        secret = token["secret_seed"].upper()
        tokens[token["name"]] = {"secret": secret, "type": "authy"}

    return tokens
