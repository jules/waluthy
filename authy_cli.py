import os
import time
import secrets
import argparse
import pyperclip
import authy_api
import authy_totp
import subprocess
from getpass import getpass
from authy_config import get_value, set_value, crypto_state, init_crypto, deinit_crypto
from urllib.parse import quote


def copy_code(code):
    if os.environ.get("XDG_SESSION_TYPE") == "wayland" or get_value(
        "force_wayland", False
    ):
        subprocess.run(["wl-copy", code])
    else:
        pyperclip.copy(code)


def cmd_get(args):
    # Load the local sync
    accs = get_value("accounts")

    if not accs:
        print("No accounts found, run sync first.")
        return

    # Get user's input
    if type(args.account) == list:
        user_input = " ".join(args.account).lower()
    else:
        user_input = args.account.lower()

    already_copied = False

    # Search through accs for matching names
    for name in accs:
        # TODO: Do fuzzy searching
        if user_input in name.lower():
            acc = accs[name]
            secret = acc["secret"]
            acc_type = acc["type"]

            if acc_type == "authy":
                code = authy_totp.gen_authy_otp(secret)
            elif acc_type == "totp":
                code = authy_totp.gen_totp(secret)
            else:
                print(f"{acc} has unknown account type: {acc_type}")
                continue

            print(f"{name}: {code}")

            # If user has copy enabled, copy the first entry only
            if not already_copied and (args.copy or get_value("force_copy", True)):
                copy_code(code)
                already_copied = True
                print(f"Copied {name}!")

            # If the value "exit_after_first_result" is set, exit
            if get_value("exit_after_first_result", False):
                break

def cmd_getsecret(args):
    # Load the local sync
    accs = get_value("accounts")

    if not accs:
        print("No accounts found, run sync first.")
        return

    # Get user's input
    if type(args.account) == list:
        user_input = " ".join(args.account).lower()
    else:
        user_input = args.account.lower()

    already_copied = False

    # Search through accs for matching names
    for name in accs:
        # TODO: Do fuzzy searching
        if user_input in name.lower():
            acc = accs[name]
            secret = acc["secret"]
            acc_type = acc["type"]

            if acc_type == "authy":
                print("This secret URI contains an Authy secret, please do not revoke Waluthy as it'll also revoke this secret.")
                code = f"otpauth://totp/{quote(name)}?secret={authy_totp.convert_secret(secret)}&digits=7&period=10"
            elif acc_type == "totp":
                code = secret
            else:
                print(f"{acc} has unknown account type: {acc_type}")
                continue

            print(f"{name}: {code}")

            # If user has copy enabled, copy the first entry only
            if not already_copied and (args.copy or get_value("force_copy", True)):
                copy_code(code)
                already_copied = True
                print(f"Copied {name}!")

            # If the value "exit_after_first_result" is set, exit
            if get_value("exit_after_first_result", False):
                break


def cmd_list(args):
    # Load the local sync
    accs = get_value("accounts")

    if not accs:
        print("No accounts found, run sync first.")
        return

    # Search through accs for matching names
    for name in accs:
        acc_type = accs[name]["type"]
        print(f"-> {name} ({acc_type})")


def cmd_crypto(args):
    # Check encryption state
    encrypted = crypto_state()

    if encrypted:
        # If it's already enabled, ensure that user wants to disable it
        print("Local encryption is currently enabled.")
        if input("Should encryption be disabled (y/N)? ").lower().strip() == "y":
            deinit_crypto()
            print("Local encryption successfully disabled.")
    else:
        # Get user's password and enable local encryption
        print("Local encryption is currently disabled.")
        init_crypto(getpass("Enter an encryption password: "))
        print("Local encryption successfully enabled.")


def cmd_sync(args):
    # Check that we're enrolled
    if not get_value("secret_totp"):
        print("No account registered, run enroll first.")
        return

    print("Syncing...")

    # Get TOTP Accounts
    # If there's no backups password, it'll be passed as None
    # which'll then get ignored by that function
    totp_accs = authy_api.authenticator_tokens(
        get_value("user_id"),
        get_value("device_id"),
        get_value("secret_totp"),
        get_value("backups_password"),
    )
    print(f"Grabbed {len(totp_accs)} TOTP accounts.")

    # Get Authy Accounts
    authy_accs = authy_api.get_authy_tokens(
        get_value("user_id"), get_value("device_id"), get_value("secret_totp")
    )
    print(f"Grabbed {len(authy_accs)} Authy accounts.")

    # Merge the dicts
    accs = {**totp_accs, **authy_accs}
    set_value("accounts", accs)
    print(f"Successfully wrote {len(accs)} accounts.")


def cmd_enroll(args):
    # Check if we're enrolled
    if get_value("secret_totp"):
        print("An account is enrolled already.")
        print("Please Ctrl+C now if you don't want to overwrite that.")

    country = input(
        "Please enter your phone number's country code (without 00 or +, like 1 or 90): "
    )
    phonenum = input(
        "Please enter your phone number without country code or preceding 0: "
    )

    acc_status = authy_api.get_account_status(country, phonenum)
    acc_status_msg = acc_status["message"]

    if acc_status_msg == "new":
        print("Actual account registration isn't implemented yet.")
        print("Please register on a mobile device if you really want to use this.")
        return
    elif acc_status_msg != "active":
        print(f"Your account stage is {acc_status_msg}, which is unknown.")
        print('It should say "active". Exiting to not cause issues.')
        return

    authy_id = acc_status["authy_id"]

    device_name = input("Device name: ")

    print("Doing enrollment by push, check your devices.")
    print("(SMS and call are currently unimplemented.)")

    # "salt", because it's not a signature^TM
    # https://twitter.com/warnvod/status/1224386661909434370
    # You're invited to reply and say hi if you're reading this line
    # it's smth like a session token, which IG could be called a signature
    salt = secrets.token_urlsafe(16)

    registration = authy_api.register_device(authy_id, salt, "push", device_name)

    reg_req_id = registration["request_id"]

    while True:
        reg_state = authy_api.get_register_state(authy_id, reg_req_id, salt)
        reg_status = reg_state["status"]

        if reg_status == "accepted":
            break
        elif reg_status != "pending":
            print(f"Got unexpected registration status of {reg_status}.")
            return

        time.sleep(1)

    reg_finalization = authy_api.finalize_register(authy_id, salt, reg_state["pin"])

    device_id = reg_finalization["device"]["id"]
    secret_totp = reg_finalization["device"]["secret_seed"]
    reg_verification = authy_api.verify_secret_seed(device_id, secret_totp)

    if not reg_verification["success"]:
        print(f"Verification failed: {reg_verification}")
        return

    set_value("user_id", authy_id)
    set_value("device_id", device_id)
    set_value("secret_totp", secret_totp)

    print("Enrollment successful. Welcome to Waluthy.")
    cmd_sync(args)


if __name__ == "__main__":
    if get_value("show_header", True):
        print("Waluthy - GPLv3 - https://gitlab.com/a/waluthy")

    parser = argparse.ArgumentParser(description="Access Authy data through CLI.")

    subparsers = parser.add_subparsers(
        help="Action to be done by program", dest="command", required=True
    )
    parser_get = subparsers.add_parser("get", help="get an account's TOTP code")
    parser_get.add_argument(
        "account", metavar="N", nargs="+", type=str, help="name of the account"
    )
    parser_get.add_argument(
        "--copy", action="store_true", help="copies OTP code to clipboard"
    )
    parser_getsecret = subparsers.add_parser("getsecret", help="get an account's TOTP secret")
    parser_getsecret.add_argument(
        "account", metavar="N", nargs="+", type=str, help="name of the account"
    )
    parser_getsecret.add_argument(
        "--copy", action="store_true", help="copies OTP secret to clipboard"
    )
    parser_list = subparsers.add_parser("list", help="list accounts")
    parser_crypto = subparsers.add_parser(
        "crypto", help="enable/disable encryption of local data"
    )
    parser_enroll = subparsers.add_parser("enroll", help="login to authy (interactive)")
    parser_sync = subparsers.add_parser("sync", help="sync data from authy")

    args = parser.parse_args()

    # TODO: clean this up
    if args.command == "get":
        cmd_get(args)
    elif args.command == "getsecret":
        cmd_getsecret(args)
    elif args.command == "crypto":
        cmd_crypto(args)
    elif args.command == "list":
        cmd_list(args)
    elif args.command == "enroll":
        cmd_enroll(args)
    elif args.command == "sync":
        cmd_sync(args)
